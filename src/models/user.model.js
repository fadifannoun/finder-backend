var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var userSchema = new Schema({
  firstName: {
    type: String,
    default: "Guest",
    required: true,
  },
  lastName: String,
  mobileNumber: {
    type: String,
    required: true,
    index: true,
    unique: true,
  },
  verificationCode: {
    type: String,
    maxLength: 4,
  },
  favoriteUsers: [
    {
      type: Schema.Types.ObjectId,
      ref: "user",
      index: true,
    },
  ],
  facebookId: String,
  twitterId: String,
  instagramId: String,
  snapchatId: String,
  tiktokId: String,
  youtubeId: String,
  githubId: String,
  linkedinId: String,
  behanceId: String,
  whatsAppId: String,
  pinterestId: String,
  twitchId: String,
  tinderId: String,
  weChatId: String,
  soundCloudId: String,
  spotifyId: String,
  telegramId: String,
  email: {
    type: String,
    //   match: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
  },
  website: String,
  occupation:String,
  followers:{
    type:Number,
    default:0,
  },
  address: String,
  isVisible: {
    type: Boolean,
    default: true,
  },
  profileImage: String,
  coverImage: String,
  lastLocationTime: Number,
  showMobileNumber: Boolean,
  geometry: {
    type: {
      type: String,
      enum: ["Point"],
    },
    coordinates: {
      type: [Number],
    },
  },
},{ autoIndex: false });

userSchema.index({ geometry: "2dsphere" },{ background: false });

const userModel = mongoose.model("user", userSchema);

module.exports = userModel;
