const userModel = require("../models/user.model");

exports.storePersonalData = async function (req, res) {
	console.log(req.body);
	console.log(req.files);
	if(!req.body.params) res.json({
		success: false,
		data: {},
		message: "data not received",
	})
	let {
		firstName,
		lastName,
		email,
		website,
		occupation,
		userId,
		showMobileNumber,
	} = JSON.parse(req.body.params);

	let files = req.files;

	let user = await userModel.findById(userId);

	let host = req.get('host')?req.get('host'):"localhost:9000";
	var url = req.protocol + '://' + host + "/uploads/";
	
	user.firstName = firstName ? firstName : user.firstName;
	user.lastName = lastName ? lastName : user.lastName;
	user.email = email ? email : user.email;
	user.website = website ? website : user.website;
	user.occupation = occupation ? occupation : user.occupation;
	user.showMobileNumber = showMobileNumber != null ? showMobileNumber : user.showMobileNumber;
	user.coverImage = files && files.coverImage ? url+files.coverImage[0].filename : user.coverImage;
	user.profileImage = files && files.profileImage ? url+files.profileImage[0].filename : user.profileImage;
	// user.mobileNumber = req.body.mobileNumber;
	user.save();
	res.send({
		success: true,
		data: {user},
		message: "",
	});
};

exports.storeSocialData = async function (req, res) {
	console.log("req body",req.body);
	console.log("req params",req.params);
	let account = req.params.account;
	let userInfo = req.body;
	console.log(userInfo.username);

	console.log(userInfo);
	if (!userInfo.mobileNumber) {
		res.send({
			success: false,
			data: {},
			message: "Mobile not found",
		});
	} else {
		let user = await userModel.findOne({ mobileNumber: userInfo.mobileNumber });
		user[`${account}Id`] = userInfo[`username`];
		if((user.firstName ==null || user.firstName == "Guest") && userInfo.firstName){
			user.firstName = userInfo.firstName;
			if(userInfo.lastName){
				user.lastName=userInfo.lastName;
			}
		}
		if((user.profileImage =="" || user.profileImage==null) && userInfo.image){
			user.profileImage=userInfo.image;
		}
		// user.firstName =
		// 	user.firstName != null && user.firstName != "Guest"
		// 		? user.firstName
		// 		: userInfo.firstName;
		// user.lastName = user.lastName ? user.lastName : userInfo.lastName;
		user.save();
		//check if successful
		res.send({
			success: true,
			data: {},
			message: "",
		});
	}
};
