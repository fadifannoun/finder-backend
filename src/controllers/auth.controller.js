const userModel = require("../models/user.model");
const { generateCode,codeMessage } = require("../services/helper-functions");

const accountSid = process.env.TWILLO_ACCOUNT_SID;
const authToken = process.env.TWILLO_AUTH_TOKEN;
const hostMobileNumber = process.env.TWILLO_HOST_MOBILE_NUMBER;
const client = require("twilio")(accountSid, authToken);

const firebase = require("firebase/app");
require("firebase/auth");

const firebaseConfig = {
  apiKey: "AIzaSyAvFtfBr9HvlWg75taC4CIc2xm3zpjLUmg",
  projectId: "findix-app",
  storageBucket: "findix-app.appspot.com",
};
firebase.initializeApp(firebaseConfig);

exports.oauth2 = function (req, res) {
	let url = "findix:/" + req.url;
	res.redirect(url);
};

exports.login = async function (req, res) {
	let recievedCode = req.body.code;
	let mobileNumber = req.body.mobileNumber;

  let user = await userModel.findOne({ mobileNumber });
  if (!user) {
    res.send({
      success: false,
      data: {},
      message: "user not found",
    });
    console.log("user not found");
    //recievedCode != user.verificationCode
  } else if (recievedCode != "1234") {
    res.send({
      success: false,
      data: {},
      message: "invalid code!",
    });
    console.log("invalid code");
  } else {
    res.send({
      success: true,
      data: { user },
      message: "",
    });
  }
};

exports.verify = async function (req, res) {
  let mobileNumber = req.body.mobileNumber;
  console.log("mobile number: ",mobileNumber);

  let code = generateCode();
  console.log("code: ",code);
  let user = await userModel.findOne({ mobileNumber });
  if (user) {
    user.verificationCode = code;
    user.save();
  } else {
    user = new userModel({
      mobileNumber,
      verificationCode: code,
    });
    user.save();
  }

  //comment this method
  //because we don't have a lot of money left
  // client.messages
  //   .create({
  //     body: "Your verification code is " + code,
  //     from: hostMobileNumber,
  //     to: mobileNumber,
  //   })
  //   .then((message) => {
  //     console.log(message)
  //   });   
  res.send({
    success: true,
    data: {},
    message: "",
  });
};

exports.verifyFirebase = async function(req,res){
  let { verificationId, smsCode } = req.body;
  console.log("credentials: ",{verificationId,smsCode});

  let credential = firebase.auth.PhoneAuthProvider.credential(
    verificationId,
    smsCode
  );

  try {
    let result = await firebase.auth().signInWithCredential(credential);
    
    console.log("verified! ", result);
    console.log(result.user);
    console.log(result.user.uid);
    console.log(result.user.phoneNumber);
    console.log(result.additionalUserInfo);
    console.log(result.additionalUserInfo.providerId);
    console.log(result.additionalUserInfo.isNewUser);
    console.log(result.operationType);

    let user = await userModel.findOne({mobileNumber:result.user.phoneNumber});
    if(!user){
      user = await userModel.create({
        mobileNumber: result.user.phoneNumber,
      })
    }

    res.send({
      success: true,
      data: {user},
      message: "verified",
    });
  } catch (e) {
    res.send({
      success: false,
      data: {},
      message: codeMessage(e.code),
    });
  }
}