var mongoose = require("mongoose");
mongoose.set("useCreateIndex", true);

const userModel = require("../models/user.model");
const { isJson } = require("../services/helper-functions");
const RequestTimeout = 300; //seconds

exports.storeLocation = async function (req, res) {
  let { location, userId } = req.body;
  console.log(location, userId);
  try {
    location = JSON.parse(location);
  } catch (error) {
    res.send({
      success: false,
      data: {},
      message: "Error in location",
    });
    return;
  }

  let geoJson = {
    type: "Point",
    coordinates: [location.longitude, location.latitude],
  };

  if (userId) {
    let user = await userModel.findById(userId);

    user.geometry = geoJson;
    user.lastLocationTime = location.time;
    user.save();
    res.send({
      success: true,
      data: {},
      message: "",
    });
  } else {
    res.send({
      success: false,
      data: {},
      message: "User not found",
    });
  }
};

exports.nearbyUsers = async function (req, res) {
  let { userId, location, distance } = req.body;
  console.log(location);

  if (!isJson(location)) {
    res.send({
      success: false,
      data: {},
      message: "Error in location",
    });
    return;
  }
  location = JSON.parse(location);
  let geoJson = {
    type: "Point",
    coordinates: [location.longitude, location.latitude],
  };

  let user;
  let favoriteUsers = [];
  if (userId != null && userId != "") {
    user = await userModel.findById(userId);
    favoriteUsers = user.favoriteUsers;
  } else {
    userId = "000000000000000000000000";
  }

  const hashMap = {};
  favoriteUsers.forEach((user) => {
    hashMap[[user._id]] = 1;
  });

  //check user request timeout?
  let users = await userModel.aggregate([
    {
      $geoNear: {
        near: geoJson,
        distanceField: "distance",
        key: "geometry",
        maxDistance: parseInt(distance),
      },
    },
    {
      $match: {
        _id: { $ne: mongoose.Types.ObjectId(userId) },
        isVisible: true,
        lastLocationTime: { $gt: Date.now() - 1000 * RequestTimeout },
      },
    },
    // {
    //   $sort:{distance:1}
    // }
  ]);

  users.forEach((user) => {
    if (hashMap[user._id]) {
      user.isFavorite = true;
    }
    if (user.showMobileNumber == false) {
      delete user.mobileNumber;
    }
  });

  res.send({
    success: true,
    data: { users },
    message: "",
  });
};

exports.handleVisible = async function (req, res) {
  let { userId, isVisible } = req.body;

  if (req.body.userId) {
    let user = await userModel.findById(userId);
    if (!user) {
      res.send({
        success: false,
        data: {},
        message: "User not found",
      });
      return;
    }
    user.isVisible = isVisible;
    user.save();

    res.send({
      success: true,
      data: {},
      message: "",
    });
  } else {
    res.send({
      success: false,
      data: {},
      message: "",
    });
  }
};

exports.handleFavorite = async function (req, res) {
  // console.log(req.body);

  let user = await userModel.findById(req.body.userId);
  if (req.body.likedUser) {
    user.favoriteUsers.push(req.body.likedUser);
    user.followers++;
    user.save();
  } else {
    //remove like
    user.favoriteUsers = user.favoriteUsers.filter(
      (user) => user != req.body.unlikedUser
    );
    user.followers--;
    user.save();
  }
  res.send({
    success: true,
    data: {followers:user.followers},
    message: "",
  });
};

exports.getFavorites = async function (req, res) {
  let mobileNumber = req.body.mobileNumber;
  console.log(mobileNumber);
  let user = await userModel.findOne({ mobileNumber });
  console.log(user.firstName);
  if (user.favoriteUsers != null && user.favoriteUsers.length > 0) {
    let populated = await user.populate("favoriteUsers").execPopulate();
    // console.log(populated.favoriteUsers);
    res.send({
      success: true,
      data: { users: populated.favoriteUsers, followers: user.followers },
      message: "",
    });
  } else {
    res.send({
      success: true,
      data: { users: [] },
      message: "",
    });
  }
};

exports.searchUsers = async function (req, res) {
  let { query } = req.body;
  const regex = new RegExp(query, "i"); // i for case insensitive
  let users = await userModel
    .aggregate([
      {
        $addFields: {
          nameFilter: {
            $concat: ["$firstName", " ", "$lastName"],
          },
        },
      },
      {
        $match: {
          $or: [
            { mobileNumber: { $regex: regex } },
            { nameFilter: { $regex: regex } },
          ],
        },
      },
    ])
    .exec();

  res.send({
    success: true,
    data: { users },
    message: "",
  });
};
