const { customAlphabet } = require("nanoid");
const nanoid = customAlphabet("1234567890", 4);

exports.generateCode = function () {
	return nanoid();
};

exports.isJson = function (obj) {
	try {
		JSON.parse(obj);
		return true;
	} catch (e) {
		return false;
	}
};

exports.codeMessage=function(code) {
  let message = "";
  switch (code) {
    case "auth/invalid-verification-id":
      message = "invalid verification id";
      break;
    case "auth/invalid-verification-code":
      message = "invalid verification code";
      break;
    case "auth/code-expired":
      message = "code expired";
      break;
    default:
      message = "something wrong happened";
  }
  return message;
}
