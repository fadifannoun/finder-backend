var express = require("express");
var router = express.Router();

var {
	storeLocation,
	nearbyUsers,
	handleVisible,
	handleFavorite,
	getFavorites,
	searchUsers,
} = require("../../controllers/user.controller");

router.use("/profile", require("./profile"));

//do they all have to be POST?

router.post("/location", storeLocation);

router.post("/nearby-users", nearbyUsers);

router.post("/visible", handleVisible);

router.post("/favorite", handleFavorite);

router.post("/favorites", getFavorites);

router.post('/search', searchUsers);

module.exports = router;
