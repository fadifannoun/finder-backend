var express = require("express");
var router = express.Router();

var { oauth2, login, verify,verifyFirebase } = require("../../controllers/auth.controller");

router.get("/", oauth2);

router.post("/login", login);

router.post("/verification-code", verify);

router.post("/firebase-auth",verifyFirebase);

module.exports = router;
