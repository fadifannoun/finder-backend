var express = require("express");
var router = express.Router();
const multer = require("multer");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    console.log("dest");
    cb(null, "uploads");
  },
  filename: function (req, file, cb) {
    console.log("filename");
    cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
  },
});

var upload = multer({ storage: storage });

var {
  storePersonalData,
  storeSocialData,
} = require("../../controllers/profile.controller");

router.post("/personal", upload.fields([{
	name: 'profileImage', maxCount: 1
  }, {
	name: 'coverImage', maxCount: 1
  }]), storePersonalData);

router.post("/social/:account", storeSocialData);

module.exports = router;
