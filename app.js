const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
require("dotenv").config();

const app = express();
const port = process.env.PORT || 9000;

// const dbURL = "mongodb://localhost:27017/";

const dbURL = `mongodb+srv://${process.env.DATABASE_USERNAME}:${process.env.DATABASE_PASSWORD}@cluster0.0gk54.mongodb.net/`;

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};
mongoose.set("useCreateIndex", true);
mongoose.connect(dbURL + "findix_db", options);

app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use("/uploads", express.static(__dirname + "/uploads"));
app.use("/auth", require("./src/routes/api/auth"));
app.use("/user", require("./src/routes/api/user"));

app.get("/", (req, res) => {
  res.send("hello");
});

app.get("/privacy-policy", (req, res) => {
  res.sendFile(__dirname + "/public/privacy-policy.html");
});

app.get("/data-deletion", (req, res) => {
  res.sendFile(__dirname + "/public/data-deletion.html");
});

app.listen(port, () => {
  console.log("server is running on port " + port);
});
